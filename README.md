# opendataschema.org

Building upon [Frictionless Data](http://frictionlessdata.io/)'s efforts. Inspired by [Schema.org](htts://schema.org) and the [JSON Schema Store](http://schemastore.org/json/).

We want to make schemas more accessible to open data producers, publishers and users.